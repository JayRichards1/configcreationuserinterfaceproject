const express = require('express');
const path = require('path');
const app = express();
var buildFolder = process.argv[2];
app.use(express.static(path.join(__dirname, '../', buildFolder)));
app.get('/*', function(req, res) {
  res.sendFile(path.join(__dirname, '../', buildFolder, 'index.html'));
});
console.log('Serving on port: 8008');
app.listen(8008);
