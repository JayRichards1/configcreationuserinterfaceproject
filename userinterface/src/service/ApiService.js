import axios from 'axios';

//this is used for services related to calling the API
class ApiService {
    //this sends a request to the API using the given URL and returns the response for use
    callApi(URL) {
        return axios.get(URL,{ crossDomain: true });
    }
}

export default new ApiService()