import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as CT from './collectionTypes.json';
import * as ACC from './ApiConnectionConfig.json';
import ApiService from './service/ApiService';
import reportWebVitals from './reportWebVitals';
import { Title } from 'fundamental-react/lib/Title';
import { FormInput } from 'fundamental-react/lib/Forms';
import { FormLabel } from 'fundamental-react/lib/Forms';
import FormRadioGroup from 'fundamental-react/lib/Forms/FormRadioGroup';
import { FormRadioItem } from 'fundamental-react/lib/Forms';
import { DatePicker } from 'fundamental-react/lib/DatePicker';
import { Button } from 'fundamental-react/lib/Button';
import LuigiClient, { addInitListener } from '@luigi-project/client';


// ====== The Title of the form ======
class FormTitle extends Component {
  render() {
    return(
      <div>
        <Title level={1}>Configuration Creation Form</Title>
      </div>
    );
  }
}

// ====== input for the runname ======
class RunNameInput extends Component {
  constructor() {
    super();
    this.state = {
      //states for the validation state and message to show when the input does or doesn't fit the requirements.
      runValidationState: "information",
      runValidationText: "The run name cannot be left blank, and must have only letters or numbers without spaces",
    }
  }

  runNameSendUp(name){
    this.props.recall(name);
  }

  /**this method validates the inputted name against the regex
   * so the user knows if the run name fits with the requirements
   * and so they know to change the run name if it doesn't
   */
  runNameValidate = (event) => {
    event.preventDefault();
    var runNameRegEx = /^[a-zA-Z0-9]+$/;
    const {value} = event.target;
    if (runNameRegEx.test(value)){
      //lets the user know that the run name is valid
      this.setState({runValidationState: "success"});
      this.setState({runValidationText: "This run name is valid"});
      this.runNameSendUp(value);
    } 
    else if (value===""){
      //lets the user know that they have left the run name blank by setting the message so they can correct it
      this.setState({runValidationState: "warning"});
      this.setState({runValidationText: "The run name cannot be left blank"});
      this.runNameSendUp("");
    }
    else {
      //lets the user know the run name is invalid and shows the rules in the message so they can correct it
      this.setState({runValidationState: "error"});
      this.setState({runValidationText: "The run name must have only letters or numbers without spaces"});
      this.runNameSendUp("Error: invalid run name input");
    }
  }

  render() {
    return(
      <div className='FormDiv1'>
        <FormLabel htmlFor='RunNameInputBox' required>
          Run Name:
        </FormLabel>
        <FormInput 
          id='RunNameInputBox'
          placeholder='Input run name'
          validationState={{state: this.state.runValidationState, text: this.state.runValidationText}}
          onChange={this.runNameValidate}
          onClick={this.runNameValidate}
        />
      </div>
    );
  }
}

// ====== Collection type selection ======
class CollectionSelection extends Component {
  constructor() {
    super();
    this.state = {
      //holds the name, description, start date, and date field for the chosen collection type for easy reference
      chosenName: "",
      chosenDesc: "",
      dateField: "",
      startDate: "",
    }
  }
  //used in part of recalling chosen options/input info so that the info can be used in the API call
  collectionSendUp = (recalledBatchSize, recalledMode, recalledFirstId, recalledLastId, recalledDateRange) =>{
    var recalledType = this.state.chosenName;
    var recalledStartDate = this.state.startDate;
    var recalledDateField = this.state.dateField;
    this.props.recall(recalledType, recalledBatchSize, recalledMode, recalledFirstId, recalledLastId, recalledDateRange, recalledStartDate, recalledDateField);
  }

  //sets the states for the collection type information for easy reference
  chooseCollectionComponent(name, desc, date, field) {
    this.setState({chosenName: name});
    this.setState({chosenDesc: desc});
    this.setState({startDate: date});
    this.setState({dateField: field});
  }

  //handles click for radio buttons, sending the information from the CollectionRadioGroup component up to CollectionSelection
  handleClickRadio(name, desc, date, field) {
    this.chooseCollectionComponent(name, desc, date, field);
  }
  render(){
    return(
      <div>
        <CollectionRadioGroup 
        types={CT.collectionTypes}
        doClickRadio={(name, desc, date, field) => this.handleClickRadio(name, desc, date, field)}
        />
        <br />
        {this.state.chosenName !== "" ? <GenericCollectionInputs description={this.state.chosenDesc} recall={this.collectionSendUp}/> : null}
      </div>
    );
  }
}

/**this generates the radio group for selecting the collection type
 * and does so using the information from the collectionTypes.json file
 * this allows easy adding of new collection types
 */
class CollectionRadioGroup extends Component {
  /**generates a FormRadioItem for every collection type in the file, 
   * and then returns a list of them to be used within the render method.
   * This all allows for procedural generation of the different options.
   */
  showRadios(radios) {
    let radioList = []
    radios.forEach(element => {
      radioList.push(
      <FormRadioItem 
      data={element.name}
      onClick={() => this.props.doClickRadio(element.name, element.description, element.startDate, element.dateField)}>
      {element.description}
      </FormRadioItem>);
    });
    return radioList;
  }

  dynamicRadio() {
    return(
      <FormRadioGroup id='collectionTypeRadioGroup' inline>{this.showRadios(this.props.types)}</FormRadioGroup>
    );
  }

  render() {
    return(
      <div>
        <FormLabel htmlFor='collectionTypeRadioGroup' required>
          Collection type:
        </FormLabel>
        {this.dynamicRadio()}
      </div>
    );
  }
}

// ====== inputs related to collection type ======
/**generic collection inputs component, takes property of the description
 * to display the specific title for the input mode.
 */
class GenericCollectionInputs extends Component {
  collectionSendUp = (recalledBatchSize, recalledMode, recalledFirstId, recalledLastId, recalledDateRange) =>{
    this.props.recall(recalledBatchSize, recalledMode, recalledFirstId, recalledLastId, recalledDateRange);
  }
  render(){
    return(
      <div>
        <Title level={3}>{this.props.description} input mode:</Title>
        <InputModeSelection recall={this.collectionSendUp}/>
      </div>
    );
  }
}

// ====== Input mode selection ======
class InputModeSelection extends Component {
  constructor() {
    super();
    this.state = {
      //holds the chosen input mode in a state for easy reference
      inputMode: "",
    }
  }

  //send up collected data so that it can be given to the API call and be submitted
  collectionSendUp = (recalledBatchSize, recalledFirstId, recalledLastId, recalledDateRange) =>{
    var recalledMode = this.state.inputMode;
    this.props.recall(recalledBatchSize, recalledMode, recalledFirstId, recalledLastId, recalledDateRange);
  }

  /**sets the state with the selected mode so that 
   * the correct inputs for that mode can be shown.
   */
  showModeComponent(Mode) {
    this.setState({inputMode: Mode});
  }

  render() {
    return(
      <div>
        <InputModeRadioGroup
        modeOfInput = {this.state.inputMode}
        doClickMode = {(Code) => this.showModeComponent(Code)}
        />
        <br />
        {this.state.inputMode === "Full" ? <FullMode recall={this.collectionSendUp}/> : null}
        {this.state.inputMode === "Date" ? <DateRangeMode recall={this.collectionSendUp}/> : null}
        {this.state.inputMode === "ID" ? <IdRangeMode recall={this.collectionSendUp}/> : null}
      </div>
    );
  }
}

/**renders the group of radio buttons for selectingthe collection mode
 * and also returns the code for the chosen mode to be used elsewhere.
 */
class InputModeRadioGroup extends Component {
  render() {
    return(
      <div>
        <FormLabel htmlFor='inputModeRadioGroup' required>Select input mode:</FormLabel>
        <FormRadioGroup id='inputModeRadioGroup' inline>
          <FormRadioItem
          data='Full'
          onClick={() => this.props.doClickMode("Full")}
          >
            Complete collection
          </FormRadioItem>

          <FormRadioItem
          data='DateRange'
          onClick={() => this.props.doClickMode("Date")}
          >
            Date range collection
          </FormRadioItem>

          <FormRadioItem
          data='IdRange'
          onClick={() => this.props.doClickMode("ID")}
          >
            ID range collection
          </FormRadioItem>
        </FormRadioGroup>
      </div>
    );
  }
}

// ====== Mode related inputs ======
//shows the inputs for the full collection mode so the user can give proper extra information
class FullMode extends Component {
  collectionSendUp(recalledBatchSize){
    var recalledFirstId = "Full mode: No ID given";
    var recalledLastId = "Full mode: No ID given";
    var recalledDateRange = "Full mode: No date range given";
    this.props.recall(recalledBatchSize, recalledFirstId, recalledLastId, recalledDateRange);
  }
  recallBatchSize = (recalledBatchSize) =>{
    this.collectionSendUp(recalledBatchSize);
  }
  render() {
    return(
      <div>
        <Title level={4}>The entire database for the chosen collection will be put in the query by date</Title>
        <DateBatchInput recall={this.recallBatchSize}/>
      </div>
    );
  }
}

//shows the inputs for the date range collection mode so the user can give proper extra information
class DateRangeMode extends Component {
  collectionSendUp(recalledBatchSize, recalledDateRange){
    var recalledFirstId = "DateRange mode: No ID given";
    var recalledLastId = "DateRange mode: No ID given";
    this.props.recall(recalledBatchSize, recalledFirstId, recalledLastId, recalledDateRange);
  }
  recallBatchSize = (recalledBatchSize) =>{
    this.collectionSendUp(recalledBatchSize, "no change");
  }
  dateRangeSendThrough = (event) =>{
    var value = event.isoFormattedDate;
    this.collectionSendUp("no change", value);
  }
  render() {
    return(
      <div>
        <Title level={4}>The database elements will be chosen by date range provided:</Title>
        <div>
          <span>
            <FormLabel required>Date range:</FormLabel>
            <DatePicker 
              enableRangeSelection
              dateFormat="DD/MM/YYYY"
              onChange={this.dateRangeSendThrough}
            />
          </span>
        </div>
        <DateBatchInput recall={this.recallBatchSize}/>
      </div>
    );
  }
}

//shows the inputs for the ID range collection mode so the user can give proper extra information
class IdRangeMode extends Component {
  collectionSendUp(recalledBatchSize, recalledFirstId, recalledLastId){
    var recalledDateRange = "IdRange mode: No date range given";
    this.props.recall(recalledBatchSize, recalledFirstId, recalledLastId, recalledDateRange);
  }
  firstIdRecall = (event) =>{
    event.preventDefault();
    const {value} = event.target;
    this.collectionSendUp("no change", value, "no change");
  }
  lastIdRecall = (event) =>{
    event.preventDefault();
    const {value} = event.target;
    this.collectionSendUp("no change", "no change", value);
  }
  batchSizeRecall = (recalledBatchSize) =>{
    this.collectionSendUp(recalledBatchSize, "no change", "no change");
  }
  render() {
    return(
      <div>
        <Title level={4}>The database elements will be chosen by ID range provided:</Title>
        <div>
          <span className='SameLine'>
            <FormLabel required>First ID:</FormLabel>
            <FormInput
              type='number'
              min='0'
              onChange={this.firstIdRecall}
            />
          </span>
          <span className='SameLine SlightOffset'>
            <FormLabel required>Last ID:</FormLabel>
            <FormInput
              type='number'
              min='0'
              onChange={this.lastIdRecall}
            />
          </span>
        </div>
        <BatchSizeInput recall={this.batchSizeRecall}/>
      </div>
    );
  }
}

/**batch size input for the ID range, setting the rules for 
 * how many or few records a user can have in a batch.
 */
class BatchSizeInput extends Component {
  batchSizeSendUp = (event) =>{
    event.preventDefault();
    var {value} = event.target;
    if (value > 10000) {
      value = 10000;
    }
    else if (value < 1) {
      value = 1;
    }
    this.props.recall(value);
  }
  render() {
    return(
      <div className='FormDiv'>
        <FormLabel htmlFor='BatchSizeSelection' required>
          Input batch size:
        </FormLabel>
        <FormInput
          id='BatchSizeSelection'
          type='number'
          min='1'
          step='1'
          max='10000'
          onChange={this.batchSizeSendUp}
        />
      </div>
    );
  }
}

/**Batch size input for dates, setting the rules for 
 * how many or few day's records can be in a batch.
 * 
 * Is used for date range and Complete collection
 */
class DateBatchInput extends Component {
  batchSizeSendUp = (event) =>{
    event.preventDefault();
    var {value} = event.target;
    if (value > 365) {
      value = 365;
    }
    else if (value < 1) {
      value = 1;
    }
    this.props.recall(value);
  }

  render() {
    return(
      <div className='FormDiv'>
        <FormLabel htmlFor='DateBatchSizeSelection' required>
          Input batch size (number of days in each batch):
        </FormLabel>
        <FormInput
          id='DateBatchSizeSelection'
          type='number'
          min='1'
          step='1'
          max='365'
          onChange={this.batchSizeSendUp}
        />
      </div>
    );
  }
}

// ====== Class for the full form ======
class EntireForm extends Component {
  constructor() {
    super();
    this.state = {
      //holds each input as states sent up so as to be easily referenced when the user wants to submit to the API.
      runName: "",
      collectionType: "",
      batchSize: "",
      collectionMode: "",
      firstId: "",
      lastId: "",
      dateField: "",
      dateRange: "",
      startDate: "",
      //holds the status code and message for use and reference once the user has clicked to submit.
      apiStatus: "N/A",
      apiMessage: "",
      //is LuigiClient initialised?
      clientInit: false,
    }

    //listen for initialised Luigi Client and set state to true for use if so.
    addInitListener(() => {
      this.setState({clientInit: true});
    });
  }

  runNameRecall = (recalledRunName) =>{
    this.setState({runName: recalledRunName});
  }

  collectionRecall = (recalledType, recalledBatchSize, recalledMode, recalledFirstId, recalledLastId, recalledDateRange, recalledStartDate, recalledDateField) =>{
    //sets state for collection type for referencing
    this.setState({collectionType: recalledType});
    //sets state for batch size for referencing
    if (recalledBatchSize !== "no change") {
      this.setState({batchSize: recalledBatchSize});
    }
    //sets state for collection mode for referencing
    this.setState({collectionMode: recalledMode});
    //sets state for first ID for referencing
    if (recalledFirstId !== "no change") {
      this.setState({firstId: recalledFirstId});
    }
    //sets state for last ID for referencing
    if (recalledLastId !== "no change") {
      this.setState({lastId: recalledLastId});
    }
    //sets state for date range for referencing
    if (recalledDateRange !== "no change") {
      this.setState({dateRange: recalledDateRange});
    }
    //sets state for start date for full for referencing
    this.setState({startDate: recalledStartDate});
    //sets state for the date field for full and date range for referencing
    this.setState({dateField: recalledDateField});
  }

  submit = () => {
    //Initalising variables from states, and a blank URL.
    //This makes it so that 'this.state.[]' doesn't have to be used multiple times.
    var URL;
    var runName = this.state.runName;
    var collectionType = this.state.collectionType;
    var batchSize = this.state.batchSize;
    var collectionMode = this.state.collectionMode;
    var dateField = this.state.dateField;
    var startDate = this.state.startDate;
    var clientInit = this.state.clientInit;
    //outputting the current value of clientInit to the console
    console.log("Client init is: " + clientInit);
    //Getting the URL prefix and address as the beginning of the URL.
    //This is to allow these to ba easily changable within a config file.
    var UrlStart = ACC.UrlPrefix+ACC.UrlAddress;
    if (runName !== "" && runName !== "Error: invalid run name input") {
      if (collectionType !== "") {
        if (batchSize !== "") {
          //Builds the correct URL for the chosen collection mode so that the API can be called correctly.
          if (collectionMode === "ID") {
            var firstId = this.state.firstId;
            var lastId = this.state.lastId;
            //build the url for the id range
            if (firstId !== "" && lastId !== "") {
              URL = 
                UrlStart + "/CreateConfigIdRange?"+
                "runName="+runName+
                "&collectionType="+collectionType+
                "&batchSize="+batchSize+
                "&firstId="+firstId+
                "&lastId="+lastId;
            }
            else {
              //error when either first or last ID has not been input
              if (firstId === "" && lastId === "") {
                if (clientInit) {
                  const settingsIDs1 = {
                    text: "E11 - Invalid first and last IDs.",
                    type: "error",
                    closeAfter: 5000
                  }
                  LuigiClient.uxManager().showAlert(settingsIDs1);
                }
                else {
                  this.setState({apiStatus: "E11"});
                  this.setState({apiMessage: "Invalid first and last IDs."});
                }
              }
              else if (lastId !== "") {
                if (clientInit) {
                  const settingsIDs2 = {
                    text: "E09 - Invalid first ID.",
                    type: "error",
                    closeAfter: 5000
                  }
                  LuigiClient.uxManager().showAlert(settingsIDs2);
                }
                else {
                  this.setState({apiStatus: "E09"});
                  this.setState({apiMessage: "Invalid first ID."});
                }
              }
              else if (firstId !== "") {
                if (clientInit) {
                  const settingsIDs3 = {
                    text: "E10 - Invalid last ID.",
                    type: "error",
                    closeAfter: 5000
                  }
                  LuigiClient.uxManager().showAlert(settingsIDs3);
                }
                else {
                  this.setState({apiStatus: "E10"});
                  this.setState({apiMessage: "Invalid last ID."});
                }
              }
              return;
            }
          }
          else if (collectionMode === "Date") {
            var dateRange = this.state.dateRange;
            //build the url for the date range
            if (dateRange !== "") {
              URL = 
                UrlStart + "/CreateConfigDateRange?"+
                "runName="+runName+
                "&collectionType="+collectionType+
                "&batchSize="+batchSize+
                "&dateField="+dateField+
                "&dateRange="+dateRange;
            }
            else {
              //error when either date range has not been input
              if (clientInit) {
                const settingsDate = {
                  text: "E05 - Invalid date Field.",
                  type: "error",
                  closeAfter: 5000
                }
                LuigiClient.uxManager().showAlert(settingsDate);
              }
              else {
                this.setState({apiStatus: "E05"});
                this.setState({apiMessage: "Invalid date Field."});
              }
              return;
            }
          }
          else if (collectionMode === "Full") {
            //build the url for the complete collection
              URL = 
                UrlStart + "/CreateConfigComplete?"+
                "runName="+runName+
                "&collectionType="+collectionType+
                "&batchSize="+batchSize+
                "&dateField="+dateField+
                "&startDate="+startDate;
          }
          //use state to choose how information is displayed
          //run API without opening in new tab/window
          if (clientInit) {
            //The client is initialised, so use alert boxes.
            //alert to prompt user to wait
            const settings1 = {
              text: "W00 - Please wait while the API works",
              type: "info",
              closeAfter: 2000
            }
            LuigiClient.uxManager().showAlert(settings1);
            //calling the API
            ApiService.callApi(URL).then(
              (response) => { //a response was returned from the API which needs handling to display for the user
                var fullStatus = Object.values(response.data);
                console.log("response: " + fullStatus[0] + " - " + fullStatus[1]);
                //decides what kind of alert it is
                var alertType;
                if (fullStatus[0].includes("E")) {
                  alertType = "error";
                }
                else if (fullStatus[0].includes("S")) {
                  alertType = "success";
                }
                //defines error settings
                const settings2 = {
                  text: fullStatus[0] + " - " + fullStatus[1],
                  type: alertType,
                  closeAfter: 5000
                };
                //shows alert on the page
                LuigiClient.uxManager().showAlert(settings2);
              }
            ).catch(
              (error) => { //an unexpected error was returned by the API
                console.log("error: " + error);
                if (error == "Error: Network Error") {
                  //defines error settings
                  const settings3 = {
                    text: "E00.3 - Unable to connect to the API.",
                    type: "error",
                    closeAfter: 5000
                  };
                  //shows alert on the page
                  LuigiClient.uxManager().showAlert(settings3);
                  //nav to connection issue page
                  LuigiClient.linkManager().navigate("/formError/connection");
                }
                else {
                  //defines error settings
                  const settings3 = {
                    text: "E00.2 - Unexpected error within API server.",
                    type: "error",
                    closeAfter: 5000
                  };
                  //shows alert on the page
                  LuigiClient.uxManager().showAlert(settings3);
                  //nav to unexpected issue page
                  LuigiClient.linkManager().navigate("/formError/unexpected");
                }
              }
            );
          }
          else {
            //the client is not initialised so set the states.
            //set message to tell user to wait
            this.setState({apiStatus: "W00"});
            this.setState({apiMessage: "Please wait while the API works."});
            ApiService.callApi(URL).then(
              (response) => { //a response was returned from the API which needs handling to display for the user
                var fullStatus = Object.values(response.data);
                console.log("response: " + fullStatus[0] + " - " + fullStatus[1]);
                this.setState({apiStatus: fullStatus[0]});
                this.setState({apiMessage: fullStatus[1]});
              }
            ).catch(
              (error) => { //an unexpected error was returned by the API
                var fullStatus = ['E00.2','Unexpected error within API server.'];
                console.log("error: " + error);
                this.setState({apiStatus: fullStatus[0]});
                this.setState({apiMessage: fullStatus[1]});
              }
            );
          }
        }
        else{
          //error when user does not select a batch size
          if (clientInit) {
            const settingsBatch = {
              text: "E04 - Invalid Batch size.",
              type: "error",
              closeAfter: 5000
            };
            LuigiClient.uxManager().showAlert(settingsBatch);
          }
          else {
            this.setState({apiStatus: "E04"});
            this.setState({apiMessage: "Invalid Batch size."});
          }
        }
      }
      else{
        //error when user does not select a collection type
        if (clientInit) {
          const settingsCollect = {
            text: "E12 - Form not completed.",
            type: "error",
            closeAfter: 5000
          };
          LuigiClient.uxManager().showAlert(settingsCollect);
        }
        else {
          this.setState({apiStatus: "E12"});
          this.setState({apiMessage: "Form not completed."});
        }
      }
    }
    else{
      //error when user does not input a valid run name
      if (clientInit) {
        const settingsRunName = {
          text: "E01 - Invalid Run Name.",
          type: "error",
          closeAfter: 5000
        };
        LuigiClient.uxManager().showAlert(settingsRunName);
      }
      else {
        this.setState({apiStatus: "E01"});
        this.setState({apiMessage: "Invalid Run Name."});
      }
    }
  }

  render() {
    LuigiClient.setTargetOrigin(ACC.TargetOrigin);
    LuigiClient.uxManager().hideLoadingIndicator();
    return(
      //rendering the different components in order
      <div className='SlightOffset'>
        <FormTitle />
        <br />
        <RunNameInput recall={this.runNameRecall}/>
        <br />
        <CollectionSelection recall={this.collectionRecall} />
        <br />
        <Button id='SubmitButton' onClick={this.submit}>Submit</Button>
        {/*only displays a status code and message when there is one available for display*/}
        {this.state.apiStatus !== "N/A" ? <p>{this.state.apiStatus} - {this.state.apiMessage}</p> : null}
      </div>
    );
  }
}

// ====== Render the Form ======
ReactDOM.render(
  <EntireForm />,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
